package org.njk.tech.crypto;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class SignatureBasedApiAuth {
  public SignatureBasedApiAuth() {
  }

  public static void main(String[] args) throws Exception {
    Security.addProvider(
        new org.bouncycastle.jce.provider.BouncyCastleProvider()
    );

    SignatureBasedApiAuth worker = new SignatureBasedApiAuth();
    PrivateKey pvtKey =  worker.readPrivateKey(new File("src/test/resources/private.nopass.pem"));
    PublicKey pubKey =  worker.readPublicKey(new File("src/test/resources/public.pem"));

    String secret = "This is a secret";
//    String token = UUID.randomUUID().toString();
    String token = "5600f6c2-24c1-47b7-9cc8-53665e5a327a";
    String userId = "namesakes";

//    String signature = worker.sign(secret, token, userId, pvtKey);
    String signature = "S8X+77SENdCrucdp5b/aCCuf6judKvZBFPIBR+m5EfF//c64Hd11iR5H0ahMSk+XMJijh6yHxgOcfpZqcvYBWxLpwT4l2ArtMFqmHE2k3QZ5mq1mXuaCdmCQKLILfCQZ2A2Uwm8U0RwzIkU77swLMTpdsombGhoa5BL4T3m0DCcDclH0skmquAyXebnAygqXURtMZ4TC92m55I1YnUNADskEpVJzrcAVZ90aTP13UaaVCEnga9lfik+SOMy5ZBcjlz0ESA18TKieqUalHeMK28tyiH9O9GzL1YLad2BiSGGigbfvtTyi9imKWvhj7jckz2QS/IpMEWWHxkl1HULaHQ==";

    worker.verify(secret, token, userId, signature, pubKey);

    String b64Encoded = "TgBhAHIAZQBzAGgAIABLAGgAYQBsAGEAcwBpAA==";
    System.out.println(new String(Base64.getDecoder().decode(b64Encoded)));
  }

  private byte[] generateSecretHash(String secret) throws NoSuchAlgorithmException {
    MessageDigest digest = MessageDigest.getInstance("SHA-256");
    return digest.digest(secret.getBytes(StandardCharsets.UTF_8));
  }

  private String sign(String message, String token, String userId, PrivateKey privateKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    Signature sig = Signature.getInstance("SHA256WithRSA");
    String secret = "vlms :: " + message + " :: " + token + " :: " + userId;
    System.out.println("Message Hash: " + Base64.getEncoder().encodeToString(generateSecretHash(secret)));
    sig.initSign(privateKey);
    sig.update(generateSecretHash(secret));
    byte[] signatureBytes = sig.sign();
    String signature = Base64.getEncoder().encodeToString(signatureBytes);
    System.out.println("X-VLMS-AuthToken-Sig: " + signature);
    return signature;
  }

  private Boolean verify(String message, String token, String userId, String signature, PublicKey publicKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException {
    String secret = "vlms :: " + message + " :: " + token + " :: " + userId;
    System.out.println("Verifying: " + secret);
    System.out.println("Message Hash: " + Base64.getEncoder().encodeToString(generateSecretHash(secret)));
    Signature sig = Signature.getInstance( "SHA256withRSA" );
    sig.initVerify( publicKey );
    sig.update(generateSecretHash(secret));
    Boolean verified = sig.verify( Base64.getDecoder().decode(signature));
    System.out.println("Verification status is: " + verified);
    return verified;
  }

  private PrivateKey readPrivateKey(File file) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    String key = new String(Files.readAllBytes(file.toPath()), Charset.defaultCharset());

    String keyContent = key
        .replace("-----BEGIN RSA PRIVATE KEY-----", "")
        .replaceAll(System.lineSeparator(), "")
        .replace("-----END RSA PRIVATE KEY-----", "")
        .trim();

    byte[] encoded = Base64.getDecoder().decode(keyContent);

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
    return keyFactory.generatePrivate(keySpec);
  }

  private PublicKey readPublicKey(File file) throws Exception {
    String key = new String(Files.readAllBytes(file.toPath()), Charset.defaultCharset());

    String publicKeyPEM = key
        .replace("-----BEGIN PUBLIC KEY-----", "")
        .replaceAll(System.lineSeparator(), "")
        .replace("-----END PUBLIC KEY-----", "");

    byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    return keyFactory.generatePublic(keySpec);
  }

}
