package org.njk.tech.crypto.jasypt

import org.jasypt.util.binary.AES256BinaryEncryptor
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.nio.file.Paths
import kotlin.random.Random


tailrec fun BufferedInputStream.encrypt(encryptor: AES256BinaryEncryptor, fos: BufferedOutputStream) {
  val buffer = ByteArray(1024)
  val len = read(buffer)
  if (len > 0) {
    fos.write(
      encryptor.encrypt(buffer)
    )
    encrypt(encryptor, fos)
  }
}

tailrec fun BufferedInputStream.decrypt(encryptor: AES256BinaryEncryptor, fos: BufferedOutputStream) {
  val buffer = ByteArray(1024)
  val len = read(buffer)
  if (len > 0) {
    fos.write(
      encryptor.decrypt(buffer)
    )
    decrypt(encryptor, fos)
  }
}

fun encryptFile(fPath: String, encryptor: AES256BinaryEncryptor): Unit =
  Paths.get(fPath).toFile().inputStream().buffered().use { inf ->
    Paths.get("$fPath.enc").toFile().outputStream().buffered().use { of ->
      inf.encrypt(encryptor, of)
    }
  }

fun decryptFile(fPath: String, encryptor: AES256BinaryEncryptor): Unit =
  Paths.get("$fPath.enc").toFile().inputStream().buffered().use { inf ->
    Paths.get("$fPath.dec").toFile().outputStream().buffered().use { of ->
      inf.decrypt(encryptor, of)
    }
  }

fun main() {
  val charPool = ('a'..'z') + ('A'..'Z') + ('0'..'9')
  val pwd = (1..16)
    .map { Random.nextInt(0, charPool.size) }
    .map(charPool::get)
    .joinToString("")

  println("Password is $pwd")
  val encryptor = AES256BinaryEncryptor().apply { setPassword(pwd) }
  encryptFile("test1.txt", encryptor)
  decryptFile("test1.txt", encryptor)

}
