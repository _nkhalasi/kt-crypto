package org.njk.tech.crypto.v2.rsa

import org.bouncycastle.util.io.pem.PemReader
import org.njk.tech.crypto.utils.B64EncodedString
import org.njk.tech.crypto.utils.fromBase64
import org.njk.tech.crypto.utils.toBase64
import java.io.StringReader
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.PublicKey
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher


@Throws(Exception::class)
fun readX509PublicKey(keyContents: String): RSAPublicKey {
  val factory: KeyFactory = KeyFactory.getInstance("RSA")

  StringReader(keyContents).use { keyReader ->
    PemReader(keyReader).use { pemReader ->
      val pemObject = pemReader.readPemObject()
      val content = pemObject.content
      val pubKeySpec = X509EncodedKeySpec(content)
      return factory.generatePublic(pubKeySpec) as RSAPublicKey
    }
  }
}

@Throws(java.lang.Exception::class)
fun readPKCS8PrivateKey(keyContents: String): RSAPrivateKey {
  val factory = KeyFactory.getInstance("RSA")

  StringReader(keyContents).use { keyReader ->
    PemReader(keyReader).use { pemReader ->
      val pemObject = pemReader.readPemObject()
      val content = pemObject.content
      val privKeySpec = PKCS8EncodedKeySpec(content)
      return factory.generatePrivate(privKeySpec) as RSAPrivateKey
    }
  }
}

@Throws(java.lang.Exception::class)
fun encrypt(text: String, key: PublicKey): B64EncodedString {
  var cipherText: ByteArray? = null

  // get an RSA cipher object and print the provider
  val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")

  // encrypt the plaintext using the public key
  cipher.init(Cipher.ENCRYPT_MODE, key)
  cipherText = cipher.doFinal(text.toByteArray(Charsets.UTF_8))
  return cipherText.toBase64()
}

@Throws(java.lang.Exception::class)
fun decrypt(text: B64EncodedString, key: PrivateKey): String {

  // decrypt the text using the private key
  val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
  cipher.init(Cipher.DECRYPT_MODE, key)
  val dectyptedText = cipher.doFinal(text.fromBase64())
  return dectyptedText.decodeToString()
}

fun main() {
  val dataToEncrypt = "4T/DVeC5StfJ5PQE8ow6peeG7I9yFKGd"

  val privateKeyPem = """-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDBjroNeORmXBVg
XZTskqFuIl/m7QDbLw37T/3MDYo2Rk0Ms0v3YVCSCbOjUvxp1CZGualYmLfWmtEo
Nk/u3tDpC1lh0mTSqoL7enUJZA9DzlpWqLsSJi0bOjUQzhj7AD0ntUKeWAj3soaV
fVHHsiazP9w/kuQbWLwhmKRw7otzrNOkChZVhb2TVIlCwt92kGfEw1UuebcibcJf
rybildQGqmxlN0cPWsbyR8RURt2z4ra7RScND9NcvzLSUvpabMvTHqQFzOYfvTht
2p2fIsJgW3BNWQPGILpcTbBV26j2F16JQCKCEtKSfr1AjppUkKCCeG3XG1Nwktph
96BoGoEZAgMBAAECggEABWECffZJgQXqfRiezs6ltBYG/9nQiL5G4pfmSo+9f76g
zJgnx9Ox5saox0bUPuvalDBzMzMJr8fQcBA+mReJSx6Gn0z7Ctbnb3rc2nDSrrWl
JskaST8qfAB8kfkNmu7DXen+5Xu/F8j5CbNph2XFsa/PougdY+kpk+48ncivCJ8i
cGZnp6eGcN3zVmjEWsP25lB7CvdAaSWQ+0K4vELGpOxyw9jWniuir6h/ZxiEoOeQ
4TRIWybK4XL0sctJp/AUGHeh2R7RuTrE2mKQwnJ6FLsL9hlesKrYiyZ2tRMhSzUC
88cA3bYv3NsVqP4c2p46CiabbkdKzoBDRXhOcheZxQKBgQDyh35/HxkrbwE+9CgO
vT+H7Hfu5Aks+r/cJLaHIyFvwZI40raIhss5JtN7OBk+q5aKg0KKeg4oB+j/DHmT
z1FwaitJk6/oTk4fO/6SG6nxz2lVq8oJ1rchQFzLfjzrCOdUESTW7p0RW8C4huFq
8Xx8owV3oqSgpQdu8YDJ1rTUUwKBgQDMTugvVE0IwhfB09U/RV0yeYCrRZdM6tHD
EUQiXC8PtBZvyOi7X88lNCzCYzEPwnLZHRfhJQw8JpiihgvRT3Fp26F+J7DFjJVU
MxgI+OkfxFEqunKTNSY9TeVBtxkoGyyIVsC/rrF/Om6wH+C+8pI+af1CE87YvQAz
uu1tfmVnYwKBgDHBVFU2Bw7fDg1hP5qOfqse+xfaVnuQ0a2TJpMgxOpNdvcLW4E0
zhpieb5sqtktkIvIE+2ezqqtvMIeSA7n2FhMn6bPkqYA8ov73eURodLG72AxBxjv
oNBEiQUCV/ML95FKlxk1CKoX/bidiz9KXXSgaS27AzUlxhmfmAFQd/abAoGARajN
v31luou63+pi9eynhxMoTL0whhg2JuVXLsPxg9AqKjj9KQ3UqeGtNSXuz2nEEMS7
pJcmIcgjGJ7NnrMQf0BUQpgzzFdPfuLg2gZMLfmYlxH1CL4BaLuLZCmSmTsSpvW1
SGW+vRtmLATicxhvxnoK0KHCT+G5FOoyE7gZyPMCgYAjyf5g/xWzP1bPvy2OVbOq
IUXS+lKT1uOH71gg9YnZL0JyXqOlB15cQfMlO11z59vYwdCdxJMOXGKS2sgUtz8c
ky+mT3/rS2yg/7MIFnXb5uos+dlsh0w0bSdp3kP42xWOezfnfML4j3fk7LRKMcbG
ohChiDbJukVMBW7yW6GDcw==
-----END PRIVATE KEY-----"""

  val publicKeyPem = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwY66DXjkZlwVYF2U7JKh
biJf5u0A2y8N+0/9zA2KNkZNDLNL92FQkgmzo1L8adQmRrmpWJi31prRKDZP7t7Q
6QtZYdJk0qqC+3p1CWQPQ85aVqi7EiYtGzo1EM4Y+wA9J7VCnlgI97KGlX1Rx7Im
sz/cP5LkG1i8IZikcO6Lc6zTpAoWVYW9k1SJQsLfdpBnxMNVLnm3Im3CX68m4pXU
BqpsZTdHD1rG8kfEVEbds+K2u0UnDQ/TXL8y0lL6WmzL0x6kBczmH704bdqdnyLC
YFtwTVkDxiC6XE2wVduo9hdeiUAighLSkn69QI6aVJCggnht1xtTcJLaYfegaBqB
GQIDAQAB
-----END PUBLIC KEY-----"""

  println("Encrypting: $dataToEncrypt")
  val encryptedKey = encrypt(dataToEncrypt, readX509PublicKey(publicKeyPem))
  println("Encrypted Key: $encryptedKey")
  val decryptedKey = decrypt(encryptedKey, readPKCS8PrivateKey(privateKeyPem))
  println("Decrypted: $decryptedKey")
}
