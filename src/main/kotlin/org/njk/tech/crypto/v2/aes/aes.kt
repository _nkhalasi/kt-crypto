package org.njk.tech.crypto.v2.aes

import org.njk.tech.crypto.utils.B64EncodedString
import org.njk.tech.crypto.utils.fromBase64
import org.njk.tech.crypto.utils.randomString
import org.njk.tech.crypto.utils.toBase64
import java.security.Security
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec


fun aes256EncryptCipher(pwd: String, iv: ByteArray, transformation: String): Cipher? =
  if (pwd.length != 32 || iv.size != 12) //To ensure 256-bit key
    null
  else {
    Cipher.getInstance(transformation).apply {
      val ivInitVector = if (transformation.contains("GCM"))
        GCMParameterSpec(128, iv)
      else
        IvParameterSpec(iv)
      init(
        Cipher.ENCRYPT_MODE,
        SecretKeySpec(pwd.toByteArray(Charsets.ISO_8859_1), "AES"),
        ivInitVector
      )
    }
  }

fun encrypt(data: String, encKey: String): B64EncodedString {
  val nonce = randomString(12)
  val iv = nonce.toByteArray(Charsets.UTF_8)
  val cipher = aes256EncryptCipher(encKey, iv, "AES/GCM/NoPadding")
  val encData = cipher!!.doFinal(data.toByteArray(Charsets.UTF_8))
  return (iv+encData).toBase64()
}

fun aes256DecryptCipher(pwd: String, iv: ByteArray, transformation: String): Cipher? =
  if (pwd.length != 32 || iv.size != 12) //To ensure 256-bit key
    null
  else {
    Cipher.getInstance(transformation).apply {
      val ivInitVector = if (transformation.contains("GCM"))
        GCMParameterSpec(128, iv)
      else
        IvParameterSpec(iv)
      init(
        Cipher.DECRYPT_MODE,
        SecretKeySpec(pwd.toByteArray(Charsets.ISO_8859_1), "AES"),
        ivInitVector
      )
    }
  }

fun decrypt(encData: B64EncodedString, encKey: String): String {
  val data = encData.fromBase64()
  val nonce = data.slice(0..11).toByteArray()
  val cipherText = data.slice(12..<data.size).toByteArray()

  val cipher = aes256DecryptCipher(encKey, nonce, "AES/GCM/NoPadding")
  val actualData = cipher!!.doFinal(cipherText)
  return actualData.decodeToString()
}


@OptIn(ExperimentalStdlibApi::class)
fun main() {
  Security.setProperty("crypto.policy", "unlimited");

  println("=========================")

  val pwd1 = randomString(32)
  println("PWD is $pwd1") //Ideally this will be arrived at from privateKey.decrypt(base64EncodePwd)
  val encryptedData = encrypt("Hello World", pwd1)
  println("Encrypted data: $encryptedData")
  println("Decoded Encrypted data: ${Base64.getDecoder().decode(encryptedData)}")
  println("Decrypted data: ${decrypt(encryptedData, pwd1)}")

  println("Decrypted from Golang: ${decrypt("fnUPTLlysLGuTcoUXozGCNwUBTprcSsGYFSdEALT0BcLGzoSsM48","4T/DVeC5StfJ5PQE8ow6peeG7I9yFKGd")}")
}
