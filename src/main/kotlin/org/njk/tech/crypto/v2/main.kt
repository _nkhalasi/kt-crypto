package org.njk.tech.crypto.v2

import org.njk.tech.crypto.utils.randomString
import org.njk.tech.crypto.v2.rsa.decrypt as rsaDecrypt
import org.njk.tech.crypto.v2.aes.decrypt as aesDecrypt
import org.njk.tech.crypto.v2.rsa.readPKCS8PrivateKey
import org.njk.tech.crypto.v2.rsa.readX509PublicKey
import org.njk.tech.crypto.v2.aes.encrypt as aesEncrypt
import org.njk.tech.crypto.v2.rsa.encrypt as rsaEncrypt


fun main() {

  val pubKeyContents = """
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3qJGwBVLkvgZsL340SKV
Xj1Wv8mxI/8mDlvMTy+pdkXZZlhk+k/z6b5sJF5n1j4bQ8xrTjxGfS03RWp6+JoF
yl4mEASfxRi1VxoCSSBP1oMoXOOVI93jg8/GvsVMwHJwUwqDSctLHIl7d72YppvO
Ph/YrEmNN7Rim9RBhHm4R5FyTljetolXWFeQpcaEnKLBMUWOwi4MulPuq7o0A8tp
Cz55VFFgJsiJJuHXVwb1zVq1/i2iiTFgmX+/L6Lj7aHivbCB9XF7bEXyCRkjn1SG
glR6R1ibk4rV2hoEu9tFN9oB7HMqiJ1F0NlG/6ssFy+FlMpkjMLEAhDUd5XLzPP+
owIDAQAB
-----END PUBLIC KEY-----
""".trimIndent()

  val privKeyContents = """
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC5J6HCwDyHHFXZ
FjUjz90BbNybh8NhYJzdkKgibnJZp0H7/LdLe7mc5LUWBiOW/yibcYWTl1z7WGnN
Kq+NnZ2a7pwxBFfEo2v7HKN/7QHf2PjCwWnNVe97ZwLiu+dhq1RHxZuwrSqeWCcJ
0Bqf0WLqWHQTxur9e203S6OlftxplgMmSYaNsudcIM2H9CThG997grVqLKrkpv8o
kCbGj0hrdPlghHUZ+mKyWptVTqPRI70jPKlyJKRldGB4RQxz46PSZZkqpOzyslz5
j1gx8TbsWol5fQXg5RMKSNnO2w9ZeX0iEm5HV72W0u94yoNdLA+Nj81oZFiLqtDQ
sdQWf7XhAgMBAAECggEAAZ1WXEfn3zfAieWXLgpEf5wu6k5LRmCczZPS24YCCkUa
nrNr98UK79LQv3HrQohUFtLtGsXhojF5rrfWExKSSsUU6JqEl2rjaKutw1JSR1zx
gmVEYm2C6p0R2d9Qm7nuQLf47Rf9fKcBJCVD7ivZxHhj/r4n03ENmUNYGHvcOTF5
W+E+t8YC10+kgFaBvtiXx7FvGo0UdNdTAIyJs/ReMcpkLQxU4lf9QZfAJraQjITb
SZYADzyNMRnUwkZOBrqb9XfVr+KgtBw+SBCbnhdTZaXGxfBKZ4BIygjOVUdvYSfC
RhocxCJlXsLZ5LgKLAP9V3HVXeyR2B2K2/t7gE2daQKBgQD8WxOpfqLXXII+3IuK
INzqyV4wXWsDWb0/95rK6zO60DWGUF49rTYm5ocjcVlFZO3e0TdM+DQ21Cf9HBzY
OJ66TP0XS4vB382FstrUSEr/n2J1BfvZgoE3ZCn54JJD9CoZNoFqZp2jT9vZ+rE3
bTNlzw1I6aOlcw7P3Du6jQp0TQKBgQC71B9sKy4/VhRzWo0TFv7X5jt0tOCPKkFj
3hIja1cBxuJKxnWIbJlpAHTXxEgfAExaM+E8kPVjMlLyNO701RPHOAGieQzPXNQX
awnEU9ZgE1SdplFQTCnxdJSE9cd93Ga3E3wFVcqsrzyrhb5ps20xRtSTYsjdyrfJ
n2xAmqbh5QKBgQDzD3ohr+KCkgy1JVGVik+eKLP624+eG4pHmWJTfjVGAxL6ul/7
AeF9mF6jSaKVT0A5c8PS8+G+yZt2k+RIckiX4l2HWXFUHA7lbr6M5tCt40M+ubO4
7ZUt1+ECV0YqA3uk6zW9WPcOfHWiP27/lsOBlOsTxk2jpu2mUVaKORzPpQKBgB8B
1qohusHD8R5V7YIgQrPbkyciEQwxWpgk+fOU7ckiTwsvFgoOe4jlBWT1Rz6u6uvd
Mqqv94KHtT+801HhtyElMsfimeXFmlG/Xt5lRLeZmVdpNHVADZCJwVbtyCAT+XTx
p4Ct+AZn62kZbCTA9R60ZbVrEmPi6lJDmjhUD4atAoGAWENXpi597u1Hwt6Fajyi
Kpp1XrBPK905vxTJqOkiFRdlCI0mGJ5vGuS3d3fjyFiHYGDGy3PkPRZjMXt+NxHT
qKoFuXXEb41RaTY3/sO3No4o37qKeyEpJLgVcnWuUPxLOGOrY8zcOSlRPDSNzJnv
+xWbXW4jIkf9fY3IdJ52bSE=
-----END PRIVATE KEY-----
""".trimIndent()

  val reqek = randomString(32)
  println("Random REQEK: $reqek")

  val clientSecret = "jVTz8mrTK9dQsdKF"
  val encryptedClientSecret = aesEncrypt(clientSecret, reqek)
  println("Encrypted Client Secret: $encryptedClientSecret")

  val requestPayload = """
[
    {
        "channel": "EMAIL",
        "to": ["bhagvat.jadeja@vayana.com"],
        "from": "donotreply@vayana.com",
        "senderName": "vayana",
        "subject": "testing",
        "body": "Hii Dev"
    }
]
""".trimIndent()
  val encryptedRequestPayload = aesEncrypt(requestPayload, reqek)
  println("Encrypted Req Payload: $encryptedRequestPayload")

  val encryptedReqek = rsaEncrypt(
    reqek, readX509PublicKey(pubKeyContents)
  )
  println("Encrypted Random Reqek: $encryptedReqek")

  println("==========================")

  val encryptedResek = "Gg7IW8/Z+dZxhI3mHZn0ClFTgY9f7gi1jJVRj33BfLQzddygqk9z5Rv9hgumhL5My8yby5w51GpTDZ9OnRih+nIdRyv/WufVRC8MU/tWe1xXTe03+qCYNbX9hGQT9yOGPmPcSoH9x+cWwrPf4fPKuPAnCrtG3tUe2c1nuc1yCEsRdtm8FBicwHP40Odie0bwmot8wEGzeN5JrgweiQar7+cJPJrjqUN3JXXwaeYEU1wyOAHVExeG5nJF2TATRLZR3xPsARRLJH6tYLfzVM30WiD2NLWbqqgpN32IbXJ1OsyV/TKL/9n00Kqzz7IxjEXQr182T4mcqw8vaPT3SoOKaA=="
  val resek = rsaDecrypt(encryptedResek, readPKCS8PrivateKey(privKeyContents))
  println("Decrypted RESEK: $resek")

  val encryptedResponse = "TUV3QlhEMzFkejdPKk8lSGft/KTS+EqUno7stMGJPOnsjwupvwxwHCscTnzFMrjro3/OhYrPPFeGYwOhw5DeIFMb914N7MxDdTmHFE6dC/cx9NsceREdofvoZWIOtwFO"
  val decryptedResponse = aesDecrypt(encryptedResponse, resek)
  println("Decrypted Response: $decryptedResponse")
}
