package org.njk.tech.crypto.utils

import java.util.*
import kotlin.random.Random

typealias B64EncodedString = String
typealias HexString = String

fun randomString(sz: Int): String {
  val charPool = ('a'..'z') + ('A'..'Z') + ('0'..'9')
  val pwd = (1..sz)
    .map { Random.nextInt(0, charPool.size) }
    .map(charPool::get)
    .joinToString("")
  return pwd
}

fun ByteArray.toBase64(): B64EncodedString =
  Base64.getEncoder().encodeToString(this@toBase64)

fun String.fromBase64(): ByteArray =
  Base64.getDecoder().decode(this@fromBase64)

fun ByteArray.hex(): HexString =
  StringBuilder().let {
    for (b in this@hex) {
      it.append(String.format("%02x", b))
    }
    it.toString()
  }
