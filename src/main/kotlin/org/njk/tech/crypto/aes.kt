package org.njk.tech.crypto

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.lang.System.out
import java.nio.file.Paths
import java.security.MessageDigest
import java.security.SecureRandom
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import kotlin.random.Random


typealias B64EncodedString = String

fun ByteArray.toBase64(): B64EncodedString =
  Base64.getEncoder().encodeToString(this@toBase64)

fun newKey(size: Int): ByteArray =
  ByteArray(size).apply { SecureRandom().nextBytes(this) }

fun salt(): B64EncodedString =
  newKey(16).toBase64()

fun iv(size: Int): B64EncodedString =
  newKey(size).toBase64()

fun nullIv(size: Int): B64EncodedString =
  Base64.getEncoder().encodeToString(
    ByteArray(size).apply {
      (0 until size).forEach {
        this[it] = "0".toByte()
      }
    }
  )

tailrec fun BufferedInputStream.encrypt(cipher: Cipher, fos: BufferedOutputStream) {
  val buffer = ByteArray(1024)
  val len = read(buffer)
  if (len > 0) {
    fos.write(
      cipher.update(buffer, 0, len)
    )
    encrypt(cipher, fos)
  }
}

tailrec fun BufferedInputStream.decrypt(cipher: Cipher, fos: BufferedOutputStream) {
  val buffer = ByteArray(1024)
  val len = read(buffer)
  if (len > 0) {
    fos.write(
      cipher.update(buffer, 0, len)
    )
    decrypt(cipher, fos)
  }
}

sealed class CipherSecret
data class Sha256Secret(val pwd: String, val encodedIv: B64EncodedString?) : CipherSecret() {
  fun toKeySpec() =
    MessageDigest.getInstance("SHA-256").let { digester ->
      val key = digester.digest(pwd.toByteArray())
      SecretKeySpec(Arrays.copyOf(key, 16), "AES")
    }
}

data class Pbkdf2Secret(val pwd: String, val encodedSalt: B64EncodedString, val encodedIv: B64EncodedString?) :
  CipherSecret() {
  fun toKeySpec() =
    PBEKeySpec(pwd.toCharArray(), Base64.getDecoder().decode(encodedSalt), 1000000, 256).let {
      val hash = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256").generateSecret(it).encoded
      SecretKeySpec(hash, "AES")
    }
}

sealed class CipherCreatorInputs(open val secret: CipherSecret, val transformation: String)
data class AesSha256CbcCipherCreatorInputs(override val secret: Sha256Secret) :
  CipherCreatorInputs(secret, "AES/CBC/PKCS5Padding")

data class AesSha256GcmCipherCreatorInputs(override val secret: Sha256Secret) :
  CipherCreatorInputs(secret, "AES/GCM/NoPadding")

data class AesPbkdf2CbcCipherCreatorInputs(override val secret: Pbkdf2Secret) :
  CipherCreatorInputs(secret, "AES/CBC/PKCS5Padding")

typealias CipherCreator<T> = (T) -> Cipher

val aesSha256CbcEncryptCipher: CipherCreator<AesSha256CbcCipherCreatorInputs> =
  { secret: AesSha256CbcCipherCreatorInputs ->
    Cipher.getInstance(secret.transformation).apply {
      val newSecret = if (secret.secret.encodedIv == null) Sha256Secret(secret.secret.pwd, iv(16))
      else Sha256Secret(secret.secret.pwd, secret.secret.encodedIv)
      init(Cipher.ENCRYPT_MODE, newSecret.toKeySpec(), IvParameterSpec(Base64.getDecoder().decode(newSecret.encodedIv)))
    }
  }

val aesSha256CbcDecryptCipher: CipherCreator<AesSha256CbcCipherCreatorInputs> =
  { secret: AesSha256CbcCipherCreatorInputs ->
    Cipher.getInstance(secret.transformation).apply {
      init(
        Cipher.DECRYPT_MODE,
        secret.secret.toKeySpec(),
        IvParameterSpec(Base64.getDecoder().decode(secret.secret.encodedIv))
      )
    }
  }

val aesSha256GcmEncryptCipher: CipherCreator<AesSha256GcmCipherCreatorInputs> =
  { secret: AesSha256GcmCipherCreatorInputs ->
    Cipher.getInstance(secret.transformation).apply {
      val newSecret = if (secret.secret.encodedIv == null) Sha256Secret(secret.secret.pwd, iv(16))
      else Sha256Secret(secret.secret.pwd, secret.secret.encodedIv)
      init(Cipher.ENCRYPT_MODE, newSecret.toKeySpec(), IvParameterSpec(Base64.getDecoder().decode(newSecret.encodedIv)))
    }
  }

val aesSha256GcmDecryptCipher: CipherCreator<AesSha256GcmCipherCreatorInputs> =
  { secret: AesSha256GcmCipherCreatorInputs ->
    Cipher.getInstance(secret.transformation).apply {
      init(
        Cipher.DECRYPT_MODE,
        secret.secret.toKeySpec(),
        IvParameterSpec(Base64.getDecoder().decode(secret.secret.encodedIv))
      )
    }
  }

val aesPbkdf2CbcEncryptCipher: CipherCreator<AesPbkdf2CbcCipherCreatorInputs> =
  { secret: AesPbkdf2CbcCipherCreatorInputs ->
    Cipher.getInstance(secret.transformation).apply {
      val newSecret = if (secret.secret.encodedIv == null) Pbkdf2Secret(secret.secret.pwd, secret.secret.encodedSalt, iv(16))
      else Pbkdf2Secret(secret.secret.pwd, secret.secret.encodedSalt, secret.secret.encodedIv)
      init(Cipher.ENCRYPT_MODE, newSecret.toKeySpec(), IvParameterSpec(Base64.getDecoder().decode(newSecret.encodedIv)))
    }
  }

val aesPbkdf2CbcDecryptCipher: CipherCreator<AesPbkdf2CbcCipherCreatorInputs> =
  { secret: AesPbkdf2CbcCipherCreatorInputs ->
    Cipher.getInstance(secret.transformation).apply {
      init(
        Cipher.DECRYPT_MODE,
        secret.secret.toKeySpec(),
        IvParameterSpec(Base64.getDecoder().decode(secret.secret.encodedIv))
      )
    }
  }



fun <T : CipherCreatorInputs> encryptFile(fpath: String, creds: T, fn: CipherCreator<T>): Unit =
  fn(creds).let { cipher ->
    Paths.get(fpath).toFile().inputStream().buffered().use { inf ->
      Paths.get("$fpath.enc").toFile().outputStream().buffered().use { of ->
        cipher.iv?.let {
//          println(Base64.getEncoder().encodeToString(it))
          of.write(it)
        }
        inf.encrypt(cipher, of)
        of.write(cipher.doFinal())
      }
    }
  }

inline fun <reified C : CipherCreatorInputs> BufferedInputStream.extractIv(creds: C) : C =
  when (creds) {
    is AesSha256CbcCipherCreatorInputs -> {
      AesSha256CbcCipherCreatorInputs(
        creds.secret.copy(
          encodedIv = Base64.getEncoder().encodeToString(
            ByteArray(16).apply { this@extractIv.read(this) }
          )
        )
      ) as C
    }
    is AesPbkdf2CbcCipherCreatorInputs -> {
      AesPbkdf2CbcCipherCreatorInputs(
        creds.secret.copy(
          encodedIv = Base64.getEncoder().encodeToString(
            ByteArray(16).apply { this@extractIv.read(this) }
          )
        )
      ) as C
    }
    else -> creds
  }

inline fun <reified T : CipherCreatorInputs> decryptFile(fpath: String, creds: T, fn: CipherCreator<T>): Unit =
  Paths.get("$fpath.enc").toFile().inputStream().buffered().use { inf ->
    Paths.get("$fpath.dec").toFile().outputStream().buffered().use { of ->
      val newCreds = inf.extractIv(creds)
      val cipher = fn(newCreds)
//      println( cipher.iv?.let { Base64.getEncoder().encodeToString(it) })
        inf.decrypt(cipher, of)
      of.write(cipher.doFinal())
    }
  }


fun main() {
  val charPool = ('a'..'z') + ('A'..'Z') + ('0'..'9')
  val pwd = (1..16)
    .map { Random.nextInt(0, charPool.size) }
    .map(charPool::get)
    .joinToString("")

  println("Password is $pwd")
//  encryptFile("test1.txt", AesSha256CbcCipherCreatorInputs(Sha256Secret(pwd, null)), aesSha256CbcEncryptCipher)
//  decryptFile("test1.txt", AesSha256CbcCipherCreatorInputs(Sha256Secret(pwd, null)), aesSha256CbcDecryptCipher)
//
//  val salt = salt()
//  val iv = iv(16)
//  encryptFile("test1.txt", AesPbkdf2CbcCipherCreatorInputs(Pbkdf2Secret(pwd, salt, iv)), aesPbkdf2CbcEncryptCipher)
//  decryptFile("test1.txt", AesPbkdf2CbcCipherCreatorInputs(Pbkdf2Secret(pwd, salt, nullIv(16))), aesPbkdf2CbcDecryptCipher)

  val secret = "URKOiqAfSlMtE2T5"
  val encKey = pwd
  val encryptor = aesSha256GcmDecryptCipher(
    AesSha256GcmCipherCreatorInputs(Sha256Secret(encKey, null))
  )

  BufferedInputStream(secret.byteInputStream()).encrypt(encryptor, BufferedOutputStream(out))
}
