package org.njk.tech.crypto.hashing.utils

import kotlin.random.Random

fun randomString(sz: Int): String {
  val charPool = ('a'..'z') + ('A'..'Z') + ('0'..'9')
  val pwd = (1..sz)
    .map { Random.nextInt(0, charPool.size) }
    .map(charPool::get)
    .joinToString("")
  return pwd
}
