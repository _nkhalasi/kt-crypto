package org.njk.tech.crypto.hashing.favrebcrypt

import at.favre.lib.crypto.bcrypt.BCrypt

fun main() {

  val secretMsg = "Secret@1234"
  val hashedMsg = BCrypt.withDefaults().hashToString(12, secretMsg.toCharArray())
  println(hashedMsg)
  val verifyResult = BCrypt.verifyer().verify(secretMsg.toCharArray(), hashedMsg)
  println("Verified: ${verifyResult.verified}")

  val hashedMsg1 = BCrypt.with(BCrypt.Version.VERSION_2B).hashToString(12, secretMsg.toCharArray())
  println(hashedMsg1)
  println("Verified: ${BCrypt.verifyer().verify(secretMsg.toCharArray(), hashedMsg1).verified}")
}
