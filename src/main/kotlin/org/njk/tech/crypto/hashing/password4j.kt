package org.njk.tech.crypto.hashing.password4j

import com.password4j.BcryptFunction
import com.password4j.Password
import com.password4j.types.Bcrypt
import org.njk.tech.crypto.hashing.utils.randomString


fun main() {

  val pepper = randomString(16)
  val secretMsg = "Secret@1234"
  val bcryptFn = BcryptFunction.getInstance(Bcrypt.B, 12)
  val hashedPwd = Password.hash(secretMsg)
                          .with(bcryptFn)
                          .result
  println(hashedPwd)

//  val hpw1 = "\$2b\$12\$mAMp811/x0mlK3acwOPfWuwkxGALWUZKtqe3Ncns3MBA.TJ38XIM2"
  val hpw1 = hashedPwd

  val verifyResult = Password.check(secretMsg, hpw1)
    .with(BcryptFunction.getInstanceFromHash(hpw1))
  println("Verified: $verifyResult")

}
