package org.njk.tech.otp.onetrickpony


import com.osmerion.onetrickpony.Base32
import com.osmerion.onetrickpony.HOTPEngine
import com.osmerion.onetrickpony.TOTPEngine
import java.nio.charset.StandardCharsets
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.Instant
import java.util.*
import kotlin.text.toByteArray


fun otpValidator(): (ByteArray, String) -> Boolean {
  val engine = HOTPEngine.builder()
    .withChecksum(false)
    .withCodeDigits(6)
    .withMacAlgorithm("HmacSHA1")
    .withTruncationOffset(HOTPEngine.USE_DYNAMIC_TRUNCATION)

  val e1 = TOTPEngine.builder()
    .configureHOTPEngine { engine.build() }
    .withTimeStep(Duration.ofSeconds(30))
    .build()

  fun validateOtp(secret: ByteArray, totp: String): Boolean =
    Instant.now().let { now ->
      when (totp) {
        e1.generate(secret, now.minusSeconds(30)) -> true
        e1.generate(secret, now.minusSeconds(60)) -> true
        e1.generate(secret, now) -> true
        e1.generate(secret, now.plusSeconds(30)) -> true
        e1.generate(secret, now.plusSeconds(60)) -> true
        else -> false
      }
    }

  return ::validateOtp
}

fun main(args: Array<String>) {
  val b32decoder = Base32.getDecoder()

  val secret = b32decoder.decode("C4LVQPEUCI3OQVG3IPSQVN3SFYDL3OMF".toByteArray(StandardCharsets.UTF_8))
  val now = Instant.now().toEpochMilli().let { nowmillis ->
    val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.00X").apply {
      timeZone = TimeZone.getTimeZone("UTC")
    }
    df.format(Date(nowmillis))
  }
  println(now.toString())
  val engine = TOTPEngine.builder()
    .configureHOTPEngine {
      it.withChecksum(false)
      it.withCodeDigits(6)
      it.withMacAlgorithm("HmacSHA1")
      it.withTruncationOffset(HOTPEngine.USE_DYNAMIC_TRUNCATION)
    }
    .withTimeStep(Duration.ofSeconds(30))
    .build()

  println(engine.generate(secret, Instant.now()))
  val totp1 = engine.generate(secret, Instant.parse(now))
  val totp2 = engine.generate(secret, Instant.now().minusSeconds(30))
  val totp3 = engine.generate(secret, Instant.now().minusSeconds(60))
  val totp4 = engine.generate(secret, Instant.now().minusSeconds(75))
  val totp41 = engine.generate(secret, Instant.now().minusSeconds(80))
  val totp42 = engine.generate(secret, Instant.now().minusSeconds(85))
  val totp43 = engine.generate(secret, Instant.now().minusSeconds(89))
  val totp44 = engine.generate(secret, Instant.now().minusSeconds(90))
  val totp50 = engine.generate(secret, Instant.now().plusSeconds(30))
  val totp51 = engine.generate(secret, Instant.now().plusSeconds(60))
  val totp52 = engine.generate(secret, Instant.now().plusSeconds(90))

  listOf(totp1, totp2, totp3, totp4, totp41, totp42, totp43, totp44, totp50, totp51, totp52).forEach {
//  listOf(totp1).forEach {
    println("OTP: $it is ${otpValidator()(secret, it)}")
  }



//  println(String(Base32.getEncoder().encode("naresh+banka@vayana.com".toByteArray())))
//  val totp5 = engine.generate("naresh@vayana.com".toByteArray(), Instant.now())
//  Thread.sleep(30000)
//  println("OTP5: $totp5 is ${otpValidator()("naresh@vayana.com".toByteArray(), totp5)}")
//  Thread.sleep(30000)
//  println("OTP5: $totp5 is ${otpValidator()("naresh@vayana.com".toByteArray(), totp5)}")
//  Thread.sleep(2000)
//  println("OTP5: $totp5 is ${otpValidator()("naresh@vayana.com".toByteArray(), totp5)}")

}
