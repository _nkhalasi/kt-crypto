package org.njk.tech.otp.atlassian

import com.atlassian.onetime.core.HMACDigest
import com.atlassian.onetime.core.OTPLength
import com.atlassian.onetime.core.TOTPGenerator
import com.atlassian.onetime.model.TOTPSecret
import java.time.Clock


fun main() {
  val secret = TOTPSecret.fromBase32EncodedString("CNJOHZO6FOX2RUK5HR4NVE4M2J5Z4KCW")
  val totpGenerator1: TOTPGenerator = TOTPGenerator()
  totpGenerator1.generateCurrent(secret).also {
    println(it)
  }

  TOTPGenerator(
    clock = Clock.systemUTC(),
    startTime = 0,
    timeStepSeconds = 30,
    otpLength = OTPLength.SIX,
    digest = HMACDigest.SHA1
  ).generateCurrent(secret).also {
    println(it)
  }

//  val x = URI("https://apps.vayana.com/simba/apis/v1/orgs/920b42ca-8746-4403-806b-279b164915e9/fi/docs/frq/3karshni###23300000008/fundables/91e10618-7804-4fe9-90cc-14be127e192a/attachments/63ccf870-33f2-486d-b080-154290e22ef4/file")
//  println(x)
}
