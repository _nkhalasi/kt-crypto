import java.security.MessageDigest
import java.security.SecureRandom
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

val b64Encoder = Base64.getEncoder()
val b64Decoder = Base64.getDecoder()

typealias B64EncodedString = String

fun salt(): B64EncodedString =
  b64Encoder.encodeToString(
    ByteArray(16).apply {
      SecureRandom().nextBytes(this)
    }
  )

// Ideally the IV should be generated during each encryption and shared with the counterparty to use during decryption.
// One way is to store the IV at the beginning of the encrypted output.
fun iv(size: Int): B64EncodedString =
  b64Encoder.encodeToString(
    ByteArray(size).apply {
      SecureRandom().nextBytes(this)
    }
  )

fun nullIv(size: Int): B64EncodedString =
  b64Encoder.encodeToString(
    ByteArray(size).apply {
      (0 until size).forEach {
        this[it] = "0".toByte()
      }
    }
  )

fun pbkdf2KeySpec(pwd: String, encodedSalt: B64EncodedString) =
  PBEKeySpec(pwd.toCharArray(), b64Decoder.decode(encodedSalt), 100000, 256).let {
    val hash = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256").generateSecret(it).encoded
    SecretKeySpec(hash, "AES")
  }

fun aesKey(pwd: B64EncodedString) =
  SecretKeySpec(b64Decoder.decode(pwd), "AES")

fun md5Secret(pwd: String): SecretKeySpec =
  MessageDigest.getInstance("SHA-256").let {
    val key = it.digest(pwd.toByteArray())
    SecretKeySpec(Arrays.copyOf(key, 16), "AES")
  }

sealed class CipherCreatorInputs
data class AesCbcCipherCreatorInputs(val pwd: String, val encodedSalt: B64EncodedString="", val encodedIv: B64EncodedString="") : CipherCreatorInputs() {
  fun withIv(encodedIv: B64EncodedString) = AesCbcCipherCreatorInputs(pwd, encodedSalt, encodedIv)
}
data class AesEcbCipherCreatorInputs(val pwd: String) : CipherCreatorInputs()
data class AesGcmCipherCreatorInputs(val pwd: String, val encodedIv: B64EncodedString=""): CipherCreatorInputs() {
  fun withIv(encodedIv: B64EncodedString) = AesGcmCipherCreatorInputs(pwd, encodedIv)
}
data class AesCtrCipherCreatorInputs(val pwd: String, val encodedSalt: B64EncodedString="", val encodedIv: B64EncodedString=""): CipherCreatorInputs() {
  fun withIv(encodedIv: B64EncodedString) = AesCtrCipherCreatorInputs(pwd, encodedSalt, encodedIv)
}

typealias CipherCreator<T> = (T) -> Cipher
val aesCbcEncryptCipher: CipherCreator<AesCbcCipherCreatorInputs> = { inp ->
  Cipher.getInstance("AES/CBC/PKCS5Padding").apply {
    init(Cipher.ENCRYPT_MODE, pbkdf2KeySpec(inp.pwd, inp.encodedSalt), IvParameterSpec(b64Decoder.decode(iv(16))))
  }
}
val aesCbcDecryptCipher: CipherCreator<AesCbcCipherCreatorInputs> = { inp ->
  Cipher.getInstance("AES/CBC/PKCS5Padding").apply {
//    init(Cipher.DECRYPT_MODE, pbkdf2KeySpec(inp.pwd, inp.encodedSalt), IvParameterSpec(b64Decoder.decode(inp.encodedIv)))
    init(Cipher.DECRYPT_MODE, aesKey(inp.pwd), IvParameterSpec(b64Decoder.decode(inp.encodedIv)))
    // init(Cipher.DECRYPT_MODE, aesKey(inp.pwd), IvParameterSpec(b64Decoder.decode(iv(16))))
  }
}

val aesEcbEncryptCipher: CipherCreator<AesEcbCipherCreatorInputs> = { inp ->
  aesKey(inp.pwd).let {
    Cipher.getInstance("AES/ECB/PKCS5Padding").apply {
      init(Cipher.ENCRYPT_MODE, it)
    }
  }
}
val aesEcbDecryptCipher: CipherCreator<AesEcbCipherCreatorInputs> = { inp ->
  aesKey(inp.pwd).let {
//    Cipher.getInstance("AES/ECB/PKCS5Padding").apply {
    Cipher.getInstance("AES/ECB/PKCS5Padding").apply {
      init(Cipher.DECRYPT_MODE, it)
    }
  }
}

fun decrypt(data: B64EncodedString, decryptor: Cipher) {
  val x =  decryptor.update(b64Decoder.decode(data))
  val y = decryptor.doFinal()
  println(String(x+y))
}

fun main(args: Array<String>) {
  println("Hello World!")

  val key = "nqGHWLiH1PKrh1uAnEGvIX24mNEFOZ8O3CVOF2U20kw="
//  val decryptor = aesCbcDecryptCipher(AesCbcCipherCreatorInputs(key, encodedIv = "wH27F1eUYNOfwjnXfVl+0g=="),)
//  decrypt("78P9rqb4+GCGgqN7wZz/1Q==", decryptor)

//  val decryptor = aesCbcDecryptCipher(AesCbcCipherCreatorInputs(key, encodedIv = "YNwtAedOmOaw1l0mgGIc0g=="))
//  decrypt("pkpCt6GmJ5ky+x+Gy/5xMw==", decryptor)


  val decryptor = aesEcbDecryptCipher(AesEcbCipherCreatorInputs(key))
  decrypt("UpPbXQj82pHkMvyuc74nUQ==", decryptor)
}
