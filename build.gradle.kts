import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "2.0.0"
  java
}

group = "me.nkhalasi"
version = "1.0-SNAPSHOT"

repositories {
  mavenCentral()
}

dependencies {
  api("org.jasypt", "jasypt", "1.9.3")
  testImplementation(kotlin("test-junit5"))
  testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
  implementation("org.bouncycastle:bcpg-jdk15on:1.68")
  implementation("com.password4j:password4j:1.8.2")
  implementation("at.favre.lib:bcrypt:0.10.2")
  implementation("com.atlassian:onetime:2.0.2")
  implementation("com.osmerion.onetrickpony:onetrickpony:1.0.0")
}

tasks.test {
  useJUnitPlatform()
}


kotlin {
  jvmToolchain {
    languageVersion.set(JavaLanguageVersion.of(21))
    vendor.set(JvmVendorSpec.AMAZON)
  }
}

